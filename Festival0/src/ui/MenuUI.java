/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author josep
 */
public class MenuUI {
   
    private final Empresa o_empresa;
    private String opcao;
    public MenuUI(Empresa empresa)
    {
        o_empresa = empresa;
    }

    void run() throws IOException
    {
        do
        {
            // menu do Festival de musicas de verao
            
            System.out.println("\n");
            System.out.println("10. Efetuar registo de um FMV");
            System.out.println("2. Associar Entidade a um FMV");
            System.out.println("3. Registar uma Entidade");
            System.out.println("4. Definir outros Responsáveis do FMV");
            System.out.println("5. Introduzir Artista");
            System.out.println("6. Especificar Programa do FMV");
            System.out.println("7. Definir Palco do FMV");
            System.out.println("8. Especificar Papel de Entidade em FMV");
            System.out.println("9. Responder Notificação para Responsável em Fmv");
            System.out.println("10. Aceitar Publicar Programa");  // nova UC
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                EfetuarRegistoFmvUI ui = new EfetuarRegistoFmvUI(o_empresa);
                ui.run();

            }
            if( opcao.equals("2") )
            {
                AssociarEntidadeFmvUI ui = new AssociarEntidadeFmvUI(o_empresa);
                ui.run();
            }
            if( opcao.equals("3") )
            {
                RegistarEntidadeUI ui = new RegistarEntidadeUI(o_empresa);
                ui.run();
            }
            
            if( opcao.equals("4") )
            {
                DefinirResponsavelUI ui = new DefinirResponsavelUI(o_empresa);
                ui.run();
            }
            
            if( opcao.equals("5") )
            {
                IntroduzirArtistaUI ui = new IntroduzirArtistaUI(o_empresa);
                ui.run();
            }
            
            if( opcao.equals("6") )
            {
                EspecificarProgramaFmvUI ui = new EspecificarProgramaFmvUI(o_empresa);
                ui.run();
            }

            if( opcao.equals("7") )
            {
                DefinirPalcoFmvUI ui = new DefinirPalcoFmvUI(o_empresa);
                ui.run();
            }
            
            if( opcao.equals("8") )
            {
                EspecificarPapelEntidadeFmvUI ui = new EspecificarPapelEntidadeFmvUI(o_empresa);
                ui.run();
            }
            
            if( opcao.equals("9") )
            {
                ResponderNotificacaoRespFmvUI ui = new ResponderNotificacaoRespFmvUI(o_empresa);
                ui.run();
            }
            
            if( opcao.equals("10") )
            {
                AceitarPublicarProgramaUI ui = new AceitarPublicarProgramaUI(o_empresa);
                ui.run();
            }
        }
        while (!opcao.equals("0") );
    }
    
}
