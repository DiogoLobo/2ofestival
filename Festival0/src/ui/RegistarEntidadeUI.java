/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.io.IOException;
import controller.RegistarEntidadeController;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author josep
 */
public class RegistarEntidadeUI {

    private final Empresa m_oEmpresa;
    private final RegistarEntidadeController m_controller;

    public RegistarEntidadeUI(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
        m_controller = new RegistarEntidadeController(oEmpresa);
    }

    public void run() throws IOException {
        System.out.println("\nNova Entidade:");
        m_controller.novaEntidade();

//        validaDados();
        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados da Entidade? (S/N)")) {
            if (m_controller.registarEntidade()) {
                System.out.println("Entidade registada.");
            } else {
                System.out.println("Entidade não registada.");
            }
        }
    }
    
    

    private void introduzDados() {
        int nif = Utils.IntFromConsole("Introduza o NIF:");
        while (Integer.toString(nif).length() != 9 || Integer.toString(nif).isEmpty()) {
            nif = Utils.IntFromConsole("Introduza o NIF corretamente:");
        }
        String designacao = Utils.readLineFromConsole("Introduza a designação: ");
        while (designacao.matches("^\\d+$") || designacao.isEmpty()) {
            designacao = Utils.readLineFromConsole("Introduza a designação corretamente: ");
        }
        String enderecoPostal = Utils.readLineFromConsole("Introduza o endereço postal: ");
        while (enderecoPostal.isEmpty()) {
            enderecoPostal = Utils.readLineFromConsole("Introduza o endereço postal corretamente: ");
        }
        String email = Utils.readLineFromConsole("Introduza o endereço eletrónico: ");
        while (email.isEmpty()) {
            email = Utils.readLineFromConsole("Introduza o endereço eletrónico corretamente: ");
        }
        String Fmv = "nenhum";
        m_controller.setDados(nif, designacao, enderecoPostal, email,Fmv);
    }

    private void apresentaDados() {
        System.out.println("\nEntidade:\n" + m_controller.getEntidadeAsString());
    }
//    private void validaDados(){
//        int nif = Utils.IntFromConsole("Introduza o NIF:");
//        while (Integer.toString(nif).length() != 9 || Integer.toString(nif).isEmpty()) {
//            nif = Utils.IntFromConsole("Introduza o NIF corretamente:");
//        }
//        String designacao = Utils.readLineFromConsole("Introduza a designação: ");
//        while (designacao.matches("^\\d+$") || designacao.isEmpty()) {
//            designacao = Utils.readLineFromConsole("Introduza a designação corretamente: ");
//        }
//        String enderecoPostal = Utils.readLineFromConsole("Introduza o endereço postal: ");
//        while (enderecoPostal.isEmpty()) {
//            enderecoPostal = Utils.readLineFromConsole("Introduza o endereço postal corretamente: ");
//        }
//        String email = Utils.readLineFromConsole("Introduza o endereço eletrónico: ");
//        while (email.isEmpty()) {
//            email = Utils.readLineFromConsole("Introduza o endereço eletrónico corretamente: ");
//        }
//    }

}
