/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.EspecificarProgramaFmvController;
import java.io.IOException;
import model.Empresa;
import model.Programa;
import utils.Utils;

/**
 *
 * @author josep
 */
public class EspecificarProgramaFmvUI {
    
    private final Empresa m_oEmpresa;
    private final EspecificarProgramaFmvController m_controller;
    
    public EspecificarProgramaFmvUI(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarProgramaFmvController(oEmpresa);
    }

    public void run() throws IOException
    {
        System.out.println("\nNovo Programa:");
        m_controller.novoPrograma();
        
//        validaDados();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Programa? (S/N)")) 
        {
            if (m_controller.registarPrograma()) {
                System.out.println("Programa registada.");
            } else {
                System.out.println("Programa não registada.");
            }
        }
    }

    private void introduzDados()
    {
        int data = Utils.IntFromConsole("Introduza a data(dd:mm:aaaa): ");
        while (Integer.toString(data).matches("[a-zA-Z]+") || Integer.toString(data).isEmpty()) {
            data = Utils.IntFromConsole("Introduza a data corretamente:");
        }
        int hora = Utils.IntFromConsole("Introduza a hora: ");
        while (Integer.toString(hora).matches("[a-zA-Z]+") || Integer.toString(hora).isEmpty()) {
            hora = Utils.IntFromConsole("Introduza a hora corretamente:");
        }
        int duracao = Utils.IntFromConsole("Introduza a duração em dias:");
        while (Integer.toString(duracao).length() != 2 || Integer.toString(duracao).isEmpty()) {
            duracao = Utils.IntFromConsole("Introduza a duração corretamente:");
        }
             
        m_controller.setDados(data,hora,duracao);
    }

    private void apresentaDados()
    {
        System.out.println("\nPrograma:\n" + m_controller.getProgramaAsString());
    }
//    private void validaDados(){
//        int data = Utils.IntFromConsole("Introduza a data:");
//        while (Integer.toString(data).length() != 9 || Integer.toString(data).isEmpty()) {
//            data = Utils.IntFromConsole("Introduza a data corretamente:");
//        }
//        int hora = Utils.IntFromConsole("Introduza a hora: ");
//         while (Integer.toString(hora).length() != 9 || Integer.toString(hora).isEmpty()) {
//            hora = Utils.IntFromConsole("Introduza a hora corretamente:");
//        }
//        int duracao = Utils.IntFromConsole("Introduza a duracao:");
//        while (Integer.toString(duracao).length() != 9 || Integer.toString(duracao).isEmpty()) {
//            duracao = Utils.IntFromConsole("Introduza a duração corretamente:");
//        }
//    }
    
}
