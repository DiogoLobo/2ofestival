/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.DefinirResponsavelController;
import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author josep
 */
public class DefinirResponsavelUI {
     private final Empresa m_oEmpresa;
    private final DefinirResponsavelController m_controller;
    
    public DefinirResponsavelUI(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new DefinirResponsavelController(oEmpresa);
    }

    public void run() throws IOException
    {
        System.out.println("\nNovo Responsável:");
        m_controller.novoResponsavel();
        
//        validaDados();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Responsavel? (S/N)")) 
        {
            if (m_controller.registarResponsavel()) {
                System.out.println("Responsável registado.");
            } else {
                System.out.println("Responsável não registado.");
            }
        }
    }

    private void introduzDados()
    {
        String responsabilidade = Utils.readLineFromConsole("Introduza a responsabilidade:");
        while (responsabilidade.length() != 9 || responsabilidade.isEmpty()) {
            responsabilidade = Utils.readLineFromConsole("Introduza o ID corretamente:");
        }
        //String denominacao = Utils.readLineFromConsole("Introduza a denominação: ");
        //int lotacao = Utils.IntFromConsole("Introduza a lotação:");
             
        m_controller.setDados(responsabilidade);
    }

    private void apresentaDados()
    {
        System.out.println("\nPalco:\n" + m_controller.getResponsavelAsString());
    }
//    private void validaDados(){
//        int id = Utils.IntFromConsole("Introduza o ID:");
//        while (Integer.toString(id).length() != 9 || Integer.toString(id).isEmpty()) {
//            id = Utils.IntFromConsole("Introduza o ID corretamente:");
//        }
//        String denominacao = Utils.readLineFromConsole("Introduza a denominação: ");
//        while (denominacao.matches("^\\d+$") || denominacao.isEmpty()) {
//            denominacao = Utils.readLineFromConsole("Introduza a denominação corretamente: ");
//        }
//        int lotacao = Utils.IntFromConsole("Introduza a Lotação:");
//        while (Integer.toString(lotacao).length() != 9 || Integer.toString(lotacao).isEmpty()) {
//            lotacao = Utils.IntFromConsole("Introduza a Lotação corretamente:");
//        }
//    }

    
    
}
