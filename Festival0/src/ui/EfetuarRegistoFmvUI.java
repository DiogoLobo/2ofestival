/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;


import java.io.IOException;
import controller.EfetuarRegistoFmvController;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author josep
 */
public class EfetuarRegistoFmvUI {
    
     private final Empresa m_oEmpresa;
    private final EfetuarRegistoFmvController m_controller;
    
    public EfetuarRegistoFmvUI(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EfetuarRegistoFmvController(oEmpresa);
    }

    public void run() throws IOException
    {
        System.out.println("\nNovo Fmv:");
        m_controller.novoFmv();
        
        introduzDados();
        
//        validaDados(); 


        apresentaDados();

        if (Utils.confirma("Confirma os dados do Fmv? S/N)")) 
        {
            if (m_controller.registarFMV()) {
                System.out.println("Fmv registado.");
            } else {
                System.out.println("Fmv não registado.");
            }
        }
    }
    
    private void introduzDados() {
        String nome = Utils.readLineFromConsole("Introduza o nome:");
        while (nome.matches("^\\d+$") || nome.isEmpty()) {
            nome = Utils.readLineFromConsole("Introduza o nome corretamente:");
        }
        String codigo = Utils.readLineFromConsole("Introduza o código: ");
         while (codigo.matches("[a-zA-Z]+") || codigo.isEmpty()) {
            codigo = Utils.readLineFromConsole("Introduza o código corretamente: ");
        }
        String desCurta = Utils.readLineFromConsole("Introduza a designação curta: ");
        while (desCurta.matches("^\\d+$") || desCurta.isEmpty()) {
            desCurta = Utils.readLineFromConsole("Introduza a designação curta corretamente: ");
        }
        String designacaoCompleta = Utils.readLineFromConsole("Introduza a designação longa: ");
        while (designacaoCompleta.matches("^\\d+$") || designacaoCompleta.isEmpty()) {
            designacaoCompleta = Utils.readLineFromConsole("Introduza a designação completa corretamente: ");
        }
        String acronimo = Utils.readLineFromConsole("Introduza o acrónimo: ");
        while (acronimo.matches("^\\d+$") || acronimo.isEmpty()) {
            acronimo = Utils.readLineFromConsole("Introduza o acrónimo corretamente: ");
        }
        String edicao = Utils.readLineFromConsole("Introduza a edição: ");
        while (edicao.matches("^\\d+$") || edicao.isEmpty()) {
            edicao = Utils.readLineFromConsole("Introduza a edição corretamente: ");
        }
       
        m_controller.setDados(nome,codigo,desCurta,designacaoCompleta,acronimo,edicao);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nFmv:\n" + m_controller.getFmvAsString());
    }

//    private void validaDados() {
//        String nome = Utils.readLineFromConsole("Introduza o nome:");
//        while (nome.matches("^\\d+$") || nome.isEmpty()) {
//            nome = Utils.readLineFromConsole("Introduza o nome corretamente:");
//        }
//        String codigo = Utils.readLineFromConsole("Introduza o código: ");
//        while (codigo.matches("[a-zA-Z]+") || codigo.isEmpty()) {
//            codigo = Utils.readLineFromConsole("Introduza o código corretamente: ");
//        }
//        String designacaoCurta = Utils.readLineFromConsole("Introduza a designação curta: ");
//        while (designacaoCurta.matches("^\\d+$") || designacaoCurta.isEmpty()) {
//            designacaoCurta = Utils.readLineFromConsole("Introduza a designação curta corretamente: ");
//        }
//        String designacaoCompleta = Utils.readLineFromConsole("Introduza a designação completa: ");
//        while (designacaoCompleta.matches("^\\d+$") || designacaoCompleta.isEmpty()) {
//            designacaoCompleta = Utils.readLineFromConsole("Introduza a designação completa corretamente: ");
//        }
//        String acronimo = Utils.readLineFromConsole("Introduza o acrónimo: ");
//        while (acronimo.matches("^\\d+$") || acronimo.isEmpty()) {
//            acronimo = Utils.readLineFromConsole("Introduza o acrónimo corretamente: ");
//        }
//        String edicao = Utils.readLineFromConsole("Introduza a edição: ");
//        while (edicao.matches("^\\d+$") || edicao.isEmpty()) {
//            edicao = Utils.readLineFromConsole("Introduza a edição corretamente: ");
//        }
//
//    }
    
}
