/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.DefinirPalcoFmvController;
import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author josep
 */
public class DefinirPalcoFmvUI {
    
    private final Empresa m_oEmpresa;
    private final DefinirPalcoFmvController m_controller;
    
    public DefinirPalcoFmvUI(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new DefinirPalcoFmvController(oEmpresa);
    }

    public void run() throws IOException
    {
        System.out.println("\nNovo Palco:");
        m_controller.novoPalco();
        
//        validaDados();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Palco? (S/N)")) 
        {
            if (m_controller.registarPalco()) {
                System.out.println("Palco registada.");
            } else {
                System.out.println("Palco não registada.");
            }
        }
    }

    private void introduzDados()
    {
        int id = Utils.IntFromConsole("Introduza o ID:");
        while (Integer.toString(id).length() != 9 || Integer.toString(id).isEmpty()) {
            id = Utils.IntFromConsole("Introduza o ID corretamente:");
        }
        String denominacao = Utils.readLineFromConsole("Introduza a denominação: ");
         while (denominacao.matches("^\\d+$") || denominacao.isEmpty()) {
            denominacao = Utils.readLineFromConsole("Introduza a denominação corretamente: ");
        }
        int lotacao = Utils.IntFromConsole("Introduza a lotação:");
        while (Integer.toString(lotacao).matches("[a-zA-Z]+") || Integer.toString(lotacao).isEmpty()) {
            lotacao = Utils.IntFromConsole("Introduza a Lotação corretamente:");
        }
             
        m_controller.setDados(id,denominacao,lotacao);
    }

    private void apresentaDados()
    {
        System.out.println("\nPalco:\n" + m_controller.getPalcoAsString());
    }
//    private void validaDados(){
//        int id = Utils.IntFromConsole("Introduza o ID:");
//        while (Integer.toString(id).length() != 9 || Integer.toString(id).isEmpty()) {
//            id = Utils.IntFromConsole("Introduza o ID corretamente:");
//        }
//        String denominacao = Utils.readLineFromConsole("Introduza a denominação: ");
//        while (denominacao.matches("^\\d+$") || denominacao.isEmpty()) {
//            denominacao = Utils.readLineFromConsole("Introduza a denominação corretamente: ");
//        }
//        int lotacao = Utils.IntFromConsole("Introduza a Lotação:");
//        while (Integer.toString(lotacao).length() != 9 || Integer.toString(lotacao).isEmpty()) {
//            lotacao = Utils.IntFromConsole("Introduza a Lotação corretamente:");
//        }
//    }
    
}
