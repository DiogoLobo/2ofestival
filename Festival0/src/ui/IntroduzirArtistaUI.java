/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.util.List;
import java.io.IOException;
import controller.IntroduzirArtistaController;
import model.Empresa;
import utils.Utils;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author josep
 */
public class IntroduzirArtistaUI {
    
    private final Empresa m_oEmpresa;
    private final IntroduzirArtistaController m_controller;
    

    public IntroduzirArtistaUI(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new IntroduzirArtistaController(oEmpresa);
    }

    void run()
    {
        System.out.println("\nNovo Artista:");
        m_controller.novoArtista();

        introduzDados();
        
        List<String> lem = m_controller.getListaEstilosMusicais();
        boolean bNew = false;
        do
        {
            String em = (String) Utils.apresentaESeleciona(lem, "Selecione o Estilo Musical:");
            m_controller.addEstiloMusical(em);

            bNew = Utils.confirma("Inserir outro estilo de música para o mesmo artista (S/N)?");

        }while(bNew);

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Artista? (S/N)")) 
        {
            if (m_controller.registarArtista()) {
                System.out.println("Artista registado.");
            } else {
                System.out.println("Artista não registado.");
            }
        }
    }

    private void introduzDados()
    {
         String nome = Utils.readLineFromConsole("Introduza o nome do artista: ");
         String designacao = Utils.readLineFromConsole("Introduza a designação: ");
         int nif = Utils.IntFromConsole("Introduza o NIF:");
         String email = Utils.readLineFromConsole("Introduza o endereço eletrónico: ");
       
        m_controller.setDados(nome,designacao,nif,email);
    }

    private void apresentaDados()
    {
        System.out.println("\nArtista:\n" + m_controller.getArtistaAsString());
    }
    private void validaDados() {
        int nif = Utils.IntFromConsole("Introduza o NIF:");
        while (Integer.toString(nif).length() != 9 || Integer.toString(nif).isEmpty()) {
            nif = Utils.IntFromConsole("Introduza o NIF corretamente:");
        }
        String nome = Utils.readLineFromConsole("Introduza o nome do artista: ");
        while (nome.matches("^\\d+$") || nome.isEmpty()) {
            nome = Utils.readLineFromConsole("Introduza o nome do artista corretamente: ");
        }
        String email = Utils.readLineFromConsole("Introduza o endereço eletrónico: ");
        while (email.isEmpty()) {
            email = Utils.readLineFromConsole("Introduza o endereço eletrónico corretamente: ");
        }

    }
}
