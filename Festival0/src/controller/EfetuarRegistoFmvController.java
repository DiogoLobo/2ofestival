/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List; 
import model.Empresa;
import model.Fmv;
/**
 *
 * @author josep
 */
public class EfetuarRegistoFmvController {
     private final Empresa m_oEmpresa;
    private Fmv m_oFmv;
    public EfetuarRegistoFmvController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }

    public void novoFmv()
    {
        this.m_oFmv = this.m_oEmpresa.novoFmv();
    }
    
    public void setDados(String Nome, String Codigo, String DesignacaoCurta, String DesignacaoCompleta, String Acronimo, String Edicao)
    {
        this.m_oFmv.setNome(Nome);
        this.m_oFmv.setCodigo(Codigo);
        this.m_oFmv.setDesignacaoCurta(DesignacaoCurta);
        this.m_oFmv.setDesignacaoCompleta(DesignacaoCompleta);
        this.m_oFmv.setAcronimo(Acronimo);
        this.m_oFmv.setEdicao(Edicao);
    }
    
    public boolean registarFMV()
    {
        return this.m_oEmpresa.registarFMV(this.m_oFmv);
    }

    public String getFmvAsString()
    {
        return this.m_oFmv.toString();
    }
    
    
}
