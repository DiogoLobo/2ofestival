/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import model.Empresa;
import utils.Utils;
import model.Responsavel;


/**
 *
 * @author josep
 */
import model.Responsavel;
public class DefinirResponsavelController {
    
    private final Empresa m_oEmpresa;
    private Responsavel m_oResponsavel;
    public DefinirResponsavelController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoResponsavel()
    {
        this.m_oResponsavel = this.m_oEmpresa.novoResponsavel();
    }
    
    public void setDados(String responsabilidade)
    {
        this.m_oResponsavel.setResponsabilidade(responsabilidade);
      
    }
    
    public boolean registarResponsavel()
    {
        return this.m_oEmpresa.registarResponsavel(this.m_oResponsavel);
    }
    
    public String getResponsavelAsString()
    {
        return this.m_oResponsavel.toString();
    }
    
    
    
}
