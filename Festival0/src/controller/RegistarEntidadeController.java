/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Empresa;
import model.Entidade;

/**
 *
 * @author josep
 */
public class RegistarEntidadeController {
    
     private final Empresa m_oEmpresa;
    private Entidade m_oEntidade;
    public RegistarEntidadeController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novaEntidade()
    {
        this.m_oEntidade = this.m_oEmpresa.novaEntidade();
    }
    
    public void setDados(int nif, String Designacao, String EnderecoPostal, String Email, String Fmv)
    {
        this.m_oEntidade.setNIF(nif);
        this.m_oEntidade.setDesignacao(Designacao);
        this.m_oEntidade.setEnderecoPostal(EnderecoPostal);
        this.m_oEntidade.setEnderecoEletronico(Email);
        this.m_oEntidade.setFmv(Fmv);
    }
    
    public boolean registarEntidade()
    {
        return this.m_oEmpresa.registarEntidade(this.m_oEntidade);
    }
    
    public String getEntidadeAsString()
    {
        return this.m_oEntidade.toString();
    }
    
}
