/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Empresa;
import model.Programa;

/**
 *
 * @author josep
 */
public class EspecificarProgramaFmvController {
    
    private final Empresa m_oEmpresa;
    private Programa m_oPrograma;
    public EspecificarProgramaFmvController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoPrograma()
    {
        this.m_oPrograma = this.m_oEmpresa.novoPrograma();
    }
    
    public void setDados(int data, int hora, int duracao)
    {
        this.m_oPrograma.setData(data);
        this.m_oPrograma.setHora(hora);
        this.m_oPrograma.setDuracao(duracao);
    }
    
    public boolean registarPrograma()
    {
        return this.m_oEmpresa.registarPrograma(this.m_oPrograma);
    }
    
    public String getProgramaAsString()
    {
        return this.m_oPrograma.toString();
    }

   
    
}
