/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Empresa;
import model.Palco;

/**
 *
 * @author josep
 */
public class DefinirPalcoFmvController {
    
    private final Empresa m_oEmpresa;
    private Palco m_oPalco;
    public DefinirPalcoFmvController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoPalco()
    {
        this.m_oPalco = this.m_oEmpresa.novoPalco();
    }
    
    public void setDados(int id, String Denominacao, int lotacao)
    {
        this.m_oPalco.setId(id);
        this.m_oPalco.setDenominacao(Denominacao);
        this.m_oPalco.setLotacao(lotacao);
    }
    
    public boolean registarPalco()
    {
        return this.m_oEmpresa.registarPalco(this.m_oPalco);
    }
    
    public String getPalcoAsString()
    {
        return this.m_oPalco.toString();
    }
}
