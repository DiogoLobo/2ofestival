/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.List;
import model.Artista;
import model.Colaborador;
import model.Empresa;

/**
 *
 * @author josep
 */
public class IntroduzirArtistaController {
    
     private final Empresa m_oEmpresa;
    private Artista m_oArtista;
    private String m_oEstiloMusical;
    public IntroduzirArtistaController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoArtista()
    {
        this.m_oArtista = this.m_oEmpresa.novoArtista();
    }
    
     public void setDados(String nome, String designacao, int nif, String email)
    { 
        this.m_oArtista.setNome(nome);
        this.m_oArtista.setDesignacao(designacao);
        this.m_oArtista.setNif(nif);
        this.m_oArtista.setEmail(email);
    }


    public List<String> getListaEstilosMusicais() 
    {
        return this.m_oEmpresa.getListaEstilosMusicais();
    }

    public void addEstiloMusical(String em)
    {
        this.m_oArtista.novoEstiloMusical(em);
    }
    
    public String getArtistaAsString()
    {
        return this.m_oArtista.toString();
    }
    
    public boolean registarArtista()
    {
        return this.m_oEmpresa.registarArtista(this.m_oArtista);
    }
    
}
