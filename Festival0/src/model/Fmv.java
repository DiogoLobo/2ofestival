/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josep
 */
public class Fmv {
    private String nome;
    private String Codigo;
    private String DesignacaoCurta;
    private String DesignacaoCompleta;
    private String Acronimo;
    private String Edicao;
    private List<Palco> listaPalcos;
   
    public Fmv()
    {
        this.listaPalcos = new ArrayList<>();
    }
    
    public Fmv(String nome, String Codigo, String DesignacaoCurta, String DesignacaoCompleta, String Acronimo, String Edicao) {
        this.setNome(nome);
        this.setCodigo(Codigo);
        this.setDesignacaoCurta(DesignacaoCurta);
        this.setDesignacaoCompleta(DesignacaoCompleta);
        this.setAcronimo(Acronimo);
        this.setEdicao(Edicao);
        this.listaPalcos = new ArrayList<>();
    }
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public final void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the cod to set
     */
    public final void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }
    
    /**
     * @return the designacaoCurta
     */
    public String getDesignacaoCurta() {
        return DesignacaoCurta;
    }

    /**
     * @param DesignacaoCurta the DesignacaoCurta to set
     */
    public final void setDesignacaoCurta(String DesignacaoCurta) {
        this.DesignacaoCurta = DesignacaoCurta;
    }

    /**
     * @return the designacaoCompleta
     */
    public String getDesignacaoCompleta() {
        return DesignacaoCompleta;
    }

    /**
     * @param designacaoCompleta the designcaoCompleta to set
     */
    public final void setDesignacaoCompleta(String designacaoCompleta) {
        this.DesignacaoCompleta = designacaoCompleta;
    }
    
    /**
     * @return the acronimo
     */
    public String getAcronimo() {
        return Acronimo;
    }

    /**
     * @param Acronimo the Acronimo to set
     */
    public final void setAcronimo(String Acronimo) {
        this.Acronimo = Acronimo;
    }
    
    /**
     * @return the edicao
     */
    public String getEdicao() {
        return Edicao;
    }

    /**
     * @param Edicao the edicao to set
     */
    public final void setEdicao(String Edicao) {
        this.Edicao = Edicao;
    }
    
    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    
    @Override
    public String toString()
    {
        return this.nome + "; " + this.Acronimo + "; " + this.Codigo + "; " + this.Edicao + ";\n" + this.DesignacaoCurta + ";\n" + this.DesignacaoCompleta + ";\n" ;
    }
    
    public List<Palco> getPalcosFMV()
    {
        return listaPalcos;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
