/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author josep
 */
public class Empresa {
    
    private final List<Responsavel> listaResponsaveis;
    private final List<Fmv> listaFestivais;
    private final List<Entidade> listaEntidades;
    private final List<Artista> listaArtistas;
    private final List<Palco> listaPalcos;
    private final List<Programa> listaProgramas;
    private final List<String> listaEstilosMusicais = Arrays.asList("Hip Hop", "Rock", "Música Clássica");

    public Empresa() {
        this.listaFestivais = new ArrayList<>();
        this.listaEntidades = new ArrayList<>();
        this.listaArtistas = new ArrayList<>();
        this.listaPalcos = new ArrayList<>();
        this.listaProgramas = new ArrayList<>();
        this.listaResponsaveis =  new ArrayList<>();
    }

    public Fmv novoFmv() {
        return new Fmv();
    }

    public boolean validaFmv(Fmv m_oFmv) {
        boolean bRet = false;
        if (m_oFmv.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registarFMV(Fmv Fmv) {
        if (this.validaFmv(Fmv)) {
            return addFmv(Fmv);
        }
        return false;
    }

    private boolean addFmv(Fmv m_oFmv) {
        return listaFestivais.add(m_oFmv);
    }

    public Entidade novaEntidade() {
        return new Entidade();
    }

    public boolean validaEntidade(Entidade m_oEntidade) {
        boolean bRet = false;
        if (m_oEntidade.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registarEntidade(Entidade m_oEntidade) {
        if (this.validaEntidade(m_oEntidade)) {
            return addEntidade(m_oEntidade);
        }
        return false;
    }

    private boolean addEntidade(Entidade m_oEntidade) {
        return listaEntidades.add(m_oEntidade);
    }

    public Artista novoArtista() {
        return new Artista();
    }

    public List<String> getListaEstilosMusicais() {
        return this.listaEstilosMusicais;
    }

    private boolean validaArtista(Artista m_oArtista) {
        boolean bRet = false;
        if (m_oArtista.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registarArtista(Artista m_oArtista) {
        if (this.validaArtista(m_oArtista)) {
            return addArtista(m_oArtista);
        }
        return false;
    }

    private boolean addArtista(Artista m_oArtista) {
        return listaArtistas.add(m_oArtista);
    }

    public Palco novoPalco() {
        return new Palco();
    }

    public boolean validaPalco(Palco m_oPalco) {
        boolean bRet = false;
        if (m_oPalco.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registarPalco(Palco m_oPalco) {
        if (this.validaPalco(m_oPalco)) {
            return addPalco(m_oPalco);
        }
        return false;
    }

    private boolean addPalco(Palco m_oPalco) {
        return listaPalcos.add(m_oPalco);
    }
    public Responsavel novoResponsavel(Responsavel Responsavel){
        return null;
        
        
    }
    
    public Programa novoPrograma() {
        return new Programa();
    }

    public boolean validaPrograma(Programa m_oPrograma) {
        boolean bRet = false;
        if (m_oPrograma.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registarPrograma(Programa m_oPrograma) {
        if (this.validaPrograma(m_oPrograma)) {
            return addPrograma(m_oPrograma);
        }
        return false;
    }

    private boolean addPrograma(Programa m_oPrograma) {
        return listaProgramas.add(m_oPrograma);
        }
    
        
        
    }


