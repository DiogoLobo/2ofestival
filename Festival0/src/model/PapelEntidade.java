/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josep
 */
public class PapelEntidade {
    private String nomePapel; 
    private List<String> ListaPapeis = new ArrayList<>();
    
    
    public PapelEntidade(String nomePapel) {
        this.nomePapel=nomePapel;
    }

    /**
     * @return the nomePapel
     */
    public String getNomePapel() {
        return nomePapel;
    }

    /**
     * @param nomePapel the nomePapel to set
     */
    public void setNomePapel(String nomePapel) {
        this.nomePapel = nomePapel;
    }

    /**
     * @return the ListaPapeis
     */
    public List<String> getListaPapeis() {
        return ListaPapeis;
    }

    /**
     * @param ListaPapeis the ListaPapeis to set
     */
    public void setListaPapeis(List<String> ListaPapeis) {
        this.ListaPapeis = ListaPapeis;
    }
   
    
}
