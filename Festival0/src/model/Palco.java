/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author josep
 */
public class Palco {
     
    private int id; 
    private String denominacao; 
    private int lotacao; 
    private ArrayList<Palco>ListaPalcos=new ArrayList<>();
    
    public Palco(int id, String denominacao, int lotacao) {
        this.id=id;
        this.denominacao=denominacao;
        this.lotacao=lotacao; 
    }

    public Palco() {
    
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the denom
     */
    public String getDenominacao() {
        return denominacao;
    }

    /**
     * @param denominacao the denom to set
     */
    public void setDenominacao(String denominacao) {
        this.denominacao = denominacao;
    }

    /**
     * @return the lotacao
     */
    public int getLotacao() {
        return lotacao;
    }

    /**
     * @param lotacao the lotacao to set
     */
    public void setLotacao(int lotacao) {
        this.lotacao = lotacao;
    }

    /**
     * @return the ListaPalcos
     */
    public ArrayList<Palco> getListaPalcos() {
        return ListaPalcos;
    }

    /**
     * @param ListaPalcos the ListaPalcos to set
     */
    public void setListaPalcos(ArrayList<Palco> ListaPalcos) {
        this.ListaPalcos = ListaPalcos;
    }

    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        
        return true;
    }
    
}
