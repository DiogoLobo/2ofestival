/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josep
 */
public class Artista {
    private int nif;
    private String Designacao;
    private String nome;
    private String email;
    private final List<String> listaEstilosMusicais;
            
    private List<String> ListaArtistas = new ArrayList<>();
    
    public Artista()
    {
        this.listaEstilosMusicais = new ArrayList<>();
    }
    
    public Artista (String nome, String Designacao,int nif, String email)
    {
        this.setNome(nome);
        this.setDesignacao(Designacao);
        this.setNif(nif);
        this.setEmail(email);
        this.listaEstilosMusicais = new ArrayList<>();
    }

    /**
     * @return the nome
     */
    public String getNome()
    {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public final void setNome(String nome)
    {
        this.nome = nome;
    }
    /**
     * @return the designacao
     */
    public String getDesignacao()
    {
        return Designacao;
    }

    /**
     * @param Designacao the des to set
     */
    public final void setDesignacao(String Designacao)
    {
        this.Designacao = Designacao;
    }
    /**
     * @return the nif
     */
    public int getNif()
    {
        return nif;
    }

    /**
     * @param nif the nif to set
     */
    public final void setNif(int nif)
    {
        this.nif = nif;
    }   

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email the email to set
     */
    public final void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * @return the EstiloMusical
     */
    public List<String> getEstiloMusical()
    {
        return listaEstilosMusicais;
    }

    /**
     * @param EstiloMusical the EstiloMusical to set
     */
    public final void addEstiloMusical(String EstiloMusical) {
        this.listaEstilosMusicais.add(EstiloMusical);
    }

    /**
     * @return the ListaArtistas
     */
    public List<String> getListaArtistas()
    {
        return ListaArtistas;
    }

    /**
     * @param ListaArtistas the ListaArtistas to set
     */
    public void setListaArtistas(List<String> ListaArtistas)
    {
        this.ListaArtistas = ListaArtistas;
    }

    public void novoEstiloMusical(String em)
    {
        addEstiloMusical(em);
    }
      
    @Override
    public String toString()
    {
        return this.nome + "; " + this.Designacao + "; " +this.nif + "; " + this.email + ";\n" + this.listaEstilosMusicais;
    }

    boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    
}
