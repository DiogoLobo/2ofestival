/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author josep
 */
public class Programa {
    
    private int data; 
    private int hora; 
    private int duracao; 
    private ArrayList<Palco>ListaPalcos=new ArrayList<>();
    private ArrayList<Artista>ListaArtistas=new ArrayList<>();
    
    public Programa(int data, int hora, int duracao) {
        this.data=data;
        this.hora=hora;
        this.duracao=duracao; 
    }

    public Programa() {
    
    }
    
   
    /**
     * @return the ListaPalcos
     */
    public ArrayList<Palco> getListaPalcos() {
        return ListaPalcos;
    }

    /**
     * @param ListaPalcos the ListaPalcos to set
     */
    public void setListaPalcos(ArrayList<Palco> ListaPalcos) {
        this.ListaPalcos = ListaPalcos;
    }

    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        
        return true;
    }

    /**
     * @return the data
     */
    public int getData() {
        return data;
    }

    /**
     * @return the hora
     */
    public int getHora() {
        return hora;
    }

    /**
     * @return the duracao
     */
    public int getDuracao() {
        return duracao;
    }

    /**
     * @return the ListaArtistas
     */
    public ArrayList<Artista> getListaArtistas() {
        return ListaArtistas;
    }

    /**
     * @param data the data to set
     */
    public void setData(int data) {
        this.data = data;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(int hora) {
        this.hora = hora;
    }

    /**
     * @param duracao the duracao to set
     */
    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    /**
     * @param ListaArtistas the ListaArtistas to set
     */
    public void setListaArtistas(ArrayList<Artista> ListaArtistas) {
        this.ListaArtistas = ListaArtistas;
    }
    
    
}
