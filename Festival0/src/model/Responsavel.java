/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author josep
 */
public class Responsavel {
    private String Nome;
    private int Telemovel;
    private String Email;
    private String Fmv;
    private ArrayList<Responsavel>listaResponsaveis=new ArrayList<>();
    
    public Responsavel(String Nome, int Telemovel, String Email, String Fmv){
        this.Nome=Nome;
        this.Telemovel=Telemovel;
        this.Email=Email;
        this.Fmv=Fmv;
        
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the Telemovel
     */
    public int getTelemovel() {
        return Telemovel;
    }

    /**
     * @param Telemovel the Telemovel to set
     */
    public void setTelemovel(int Telemovel) {
        this.Telemovel = Telemovel;
    }

    /**
     * @return the Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email the Email to set
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return the Fmv
     */
    public String getFmv() {
        return Fmv;
    }

    /**
     * @param Fmv the Fmv to set
     */
    public void setFmv(String Fmv) {
        this.Fmv = Fmv;
    }
}
