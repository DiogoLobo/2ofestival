/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author josep
 */
public class Entidade {
    private int nif;
    private String Designacao;
    private String EnderecoPostal;
    private String email;
    private String Fmv;
    
    
    public Entidade(int nif, String Designacao, String EnderecoPostal, String email, String Fmv)
    {
        this.setNIF(nif);
        this.setDesignacao(Designacao);
        this.setEnderecoPostal(EnderecoPostal);
        this.setEnderecoEletronico(email);
        this.Fmv=Fmv;
    }

    public Entidade()
    {
        
    }
    /**
     * @return the nif
     */
    public int getNIF() {
        return nif;
    }

    /**
     * @param nif the nif to set
     */
    public final void setNIF(int nif) {
        this.nif = nif;
    }

    /**
     * @return the des
     */
    public String getDesignacao() {
        return Designacao;
    }

    /**
     * @param Designacao the des to set
     */
    public final void setDesignacao(String Designacao) {
        this.Designacao = Designacao;
    }

    /**
     * @return the enderecoPostal
     */
    public String getEnderecoPostal() {
        return EnderecoPostal;
    }

    /**
     * @param EnderecoPostal the enderecoPostal to set
     */
    public final void setEnderecoPostal(String EnderecoPostal) {
        this.EnderecoPostal = EnderecoPostal;
    }

    /**
     * @return the email
     */
    public String getEnderecoEletronico() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public final void setEnderecoEletronico(String email) {
        this.email = email;
    }
    
    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        
        return true;
    }
    /**
     * @return the Fmv
     */
    public String getFmv() {
        return Fmv;
    }

    /**
     * @param Fmv the Fmv to set
     */
    public void setFmv(String Fmv) {
        this.Fmv = Fmv;
    }
    @Override
    public String toString()
    {
        return this.nif + "; " + this.Designacao + ";\n" + this.EnderecoPostal + "; " + this.email + ";";
    }

    
}
